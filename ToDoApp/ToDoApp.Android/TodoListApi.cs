﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using Android.App;
//using Android.Content;
//using Android.OS;
//using Android.Runtime;
//using Android.Views;
//using Android.Widget;
//using Newtonsoft.Json;
//using System.Net.Http;

//namespace ToDoApp.Droid
//{
//    class TodoListApi
//    {
//        public List<TodoListEntity> articleList;

//        // APIのURl
//        public string API_URL_TODOS = "https://frontec-todo-app-api.com/api/users/1/todos";

//        // データを取得するメソッド
//        public async Task<List<TodoListEntity>> AsyncGetWebAPIData()
//        {
//            // Listの作成
//            articleList = new List<TodoListEntity>();
//            // HttpClientの作成 
//            HttpClient httpClient = new HttpClient();
//            // 非同期でAPIからデータを取得
//            Task<string> stringAsync = httpClient.GetStringAsync(API_URL_TODOS);
//            string result = await stringAsync;
//            // JSON形式のデータをデシリアライズ
//            articleList = JsonConvert.DeserializeObject<List<TodoListEntity>>(result);

//            // List でデータを返す
//            return articleList;
//        }

//    }

//    // QiitaApiから取得するデータのEntity
//    public class TodoListEntity
//    {
//        //id: 2,
//        //title: "todo2",
//        //memo: "memo2",
//        //deadline: "2019-05-13",
//        //created_at: "2019-05-14T06:17:13.898Z",
//        //updated_at: "2019-05-14T06:17:13.898Z",
//        //completed: false,
//        //user_id: 1


//        public int id
//        {
//            get; set;
//        }
//        public string title
//        {
//            get; set;
//        }
//        public string memo
//        {
//            get; set;
//        }
//        public DateTime deadline
//        {
//            get; set;
//        }
//        public DateTime created_at
//        {
//            get; set;
//        }
//        public DateTime updated_at
//        {
//            get; set;
//        }
//        public bool completed
//        {
//            get; set;
//        }
//        public int user_id
//        {
//            get; set;
//        }
//    }
//}