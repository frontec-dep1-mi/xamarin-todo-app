﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ToDoApp
{
    /// <summary>
    /// TODO詳細ページ
    /// </summary>
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Detail : ContentPage
	{
        public Detail(Task task)
        {
            InitializeComponent();

            TaskNameLabel.Text = task.TaskName + "の詳細ページです";

        }

    }
}