﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ToDoApp
{
    /// <summary>
    /// グルーピング
    /// </summary>
    public class GroupingItem : List<Task>
    {
        public string SectionLabel{ get; set;}

        public GroupingItem(string sectionLabel)
        {
            this.SectionLabel = sectionLabel;
        }
    }

    /// <summary>
    /// タスク
    /// </summary>
    public class Task : ObservableCollection<string>
    {
        public string TaskName{ get; set;}
        public DateTime? Deadline{ get; set;}
        public bool Completed{ get; set;}

        #region Styles

        /// <summary>
        /// 締め切りが今日
        /// </summary>
        public bool IsDeadlineToday => (Deadline != null && Deadline <= DateTime.Parse("2019/5/31"));

        /// <summary>
        /// 締め切りが今日の場合は赤、そうでない場合は灰色
        /// </summary>
        public Color DeadLineTextColor => (IsDeadlineToday ? Color.Red : Color.Gray);

        #endregion

        public Task(string taskName, DateTime? deadline, bool completed)
        {
            this.TaskName = taskName;
            this.Deadline = deadline;
            this.Completed = completed;
        }
    }

    /// <summary>
    /// TOPページ（TODO一覧)クラス
    /// </summary>
    public partial class MainPage : ContentPage
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public MainPage()
        {
            InitializeComponent();

            // ListViewのソースを設定
            SetTodoListItemSource();

            listView.ItemSelected += (sender, e) =>
            {
                Navigation.PushAsync(new Detail((Task)e.SelectedItem));
            };

            ToolbarItems.Add(new ToolbarItem { Icon= "add_white_24dp.png", Command = new Command(() => DisplayAlert("登録処理", "未実装です", "OK")) });
        }
        

        /// <summary>
        /// データ取得のメソッドを実行しListViewのソースにセット 
        /// </summary>
        private void SetTodoListItemSource()
        {
            try
            {
                // 取得したデータをListに設定
                List<TodoListEntity> todoList = new TodoListApi().GetTodoList();

                // 日付を取得
                var today = DateTime.Parse("2019/5/31"); // DateTime.Today;
                var nextWeek = today.AddDays(7);

                var groupToday = new GroupingItem("今日");
                var groupNextWeek = new GroupingItem("1週間");
                var groupSomeDay = new GroupingItem("いつかやる");

                void SetTodoGroup(GroupingItem group, List< TodoListEntity> groupedTodo)
                {
                    foreach (TodoListEntity todo in groupedTodo)
                    {
                        group.Add(new Task(todo.title, todo.deadline, todo.completed));
                    }
                }

                SetTodoGroup(groupToday, todoList.Where(r => r.deadline < today && !r.completed).ToList());
                SetTodoGroup(groupNextWeek, todoList.Where(r => today < r.deadline && r.deadline < nextWeek && !r.completed).ToList());
                SetTodoGroup(groupSomeDay, todoList.Where(r => (r.deadline == null || nextWeek < r.deadline) && !r.completed).ToList());

                // ListViewにセット
                listView.ItemsSource = new ObservableCollection<GroupingItem>() { groupToday, groupNextWeek, groupSomeDay };
            }
            // エラー表示処理
            catch (System.Exception ex)
            {
                DisplayAlert("Error", ex.Message.ToString(), "OK");
            }

        }

        #region 非同期処理の場合

        //// 非同期でデータ取得のメソッドを実行しListViewのソースにセット
        //async void SetTodoListItemSource()
        //{
        //    try
        //    {
        //        // 取得したデータをListに設定
        //        List<TodoListEntity> todoList = await new TodoListApi().AsyncGetTodoList();

        //        // 日付を取得
        //        var today = DateTime.Parse("2019/5/31"); // DateTime.Today;
        //        var nextWeek = today.AddDays(7);

        //        var groupToday = new GroupingItem("今日");
        //        foreach (TodoListEntity todo in todoList.Where(r => r.deadline < today && !r.completed))
        //        {
        //            groupToday.Add(new Task(todo.title, todo.deadline, todo.completed));
        //        }

        //        var groupNextWeek = new GroupingItem("1週間");
        //        foreach (TodoListEntity todo in todoList.Where(r => today < r.deadline && r.deadline < nextWeek && !r.completed))
        //        {
        //            groupNextWeek.Add(new Task(todo.title, todo.deadline, todo.completed));
        //        }

        //        var groupSomeDay = new GroupingItem("いつかやる");
        //        foreach (TodoListEntity todo in todoList.Where(r => (r.deadline == null || nextWeek < r.deadline) && !r.completed))
        //        {
        //            groupSomeDay.Add(new Task(todo.title, todo.deadline, todo.completed));
        //        }

        //        // ListViewにセット
        //        listView.ItemsSource = new ObservableCollection<GroupingItem>() { groupToday, groupNextWeek, groupSomeDay };
        //    }
        //    // エラー表示処理
        //    catch (System.Exception ex)
        //    {
        //        await DisplayAlert("Error", ex.Message.ToString(), "OK");
        //    }

        //}

        #endregion

    }
}
